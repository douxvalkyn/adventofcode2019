package day22;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import outils.Outil;

public class Day22 {

	// main -----
	public static void main(String[] args) throws IOException  {
		System.out.println("["+Day22.class.getSimpleName()+"]");
		Day22 day = new Day22();
		LocalDateTime start = LocalDateTime.now();
		// day.run2();
		day.run1bis();
		LocalDateTime end = LocalDateTime.now();
		Duration duree = Duration.between(start, end);
		System.out.println("[Time: " + duree.getSeconds() + " s]");
	}

	
	// run1 ----
	public void run1bis() throws IOException {
		List<String> input = lectureInput();		
		long factoryOrder=10;
		long positionASuivre=2020L;
		long indice=shufflingReverse( input, positionASuivre, factoryOrder);
		System.out.println("indice "+positionASuivre + ",provient de indice: "+indice);
	}
	
	
	
	private long shufflingReverse( List<String> input, long positionASuivre, long factoryOrder) {
		for (int i=input.size()-1;i>=0;i--) {
			String arg0 = StringUtils.split(input.get(i), " ")[0];
			String arg1 = StringUtils.split(input.get(i), " ")[1];
			String shuffleMethod="";
			if (arg0.equals("cut")) {
				shuffleMethod="cut";
			}else {
				if (arg1.equals("with")) {
					shuffleMethod="deals with";
				}else {
					shuffleMethod="deals into";
				}
			}
		
		switch (shuffleMethod) {
		case "cut":
			positionASuivre=shuffleCutReverse(factoryOrder,input.get(i), arg1, positionASuivre);
			break;
		case "deals with":
			positionASuivre=shuffleDealsWithIncrementReverse(factoryOrder,input.get(i), Integer.valueOf(StringUtils.split(input.get(i), " ")[3]), positionASuivre);
			break;
		case "deals into":
			positionASuivre=shuffleDealsIntoReverse(factoryOrder,input.get(i), positionASuivre);
			break;

		default:
			break;
		}
		}
		return positionASuivre;
	}


	private long shuffleDealsWithIncrementReverse(long factoryOrder, String input, Integer n,	long positionASuivre) {
		int x=0;
		boolean go=true;
		while (go) {
			//on verifie si l'expression suivante est un nombre entier ou non
			double test = ((positionASuivre+x*factoryOrder)/(1.0*n)) ;
			int test2=(int) test;
			
			if (test == test2){
				go=false;
			}else {
				x++;
			}
		}
		return ((positionASuivre+x*factoryOrder)/n);
	}


	private long shuffleCutReverse(long factoryOrder, String string, String arg1, long positionASuivre) {
		if (Integer.valueOf(arg1)>0) {
		return (positionASuivre+Integer.valueOf(arg1))%factoryOrder;
		}else {
			return (positionASuivre+ factoryOrder+ Integer.valueOf(arg1))%factoryOrder;
		}
	}


	private long shuffleDealsIntoReverse(long factoryOrder, String string, long positionASuivre) {
		return factoryOrder-1-positionASuivre;
	}


	// run1 ----
	public void run1() throws IOException {
		List<String> input = lectureInput();		
		List<Integer> deck = new ArrayList<Integer>();
		long factoryOrder=10007L;
		creationDeck(deck, factoryOrder);
		deck=shuffling(deck, input);
		int indice=rechercheIndiceCarte(deck,2019);
		System.out.println(indice);
	}


	private int rechercheIndiceCarte(List<Integer> deck, int objectif) {
		for (int i=0;i<deck.size();i++) {
			if (deck.get(i)==objectif) {
				return i;
			}
		}
		return -1;
	}


	private List<Integer> shuffling(List<Integer> deck, List<String> input) {
		for (String shuffle: input) {
			String arg0 = StringUtils.split(shuffle, " ")[0];
			String arg1 = StringUtils.split(shuffle, " ")[1];
			String shuffleMethod="";
			if (arg0.equals("cut")) {
				shuffleMethod="cut";
			}else {
				if (arg1.equals("with")) {
					shuffleMethod="deals with";
				}else {
					shuffleMethod="deals into";
				}
			}
			
		switch (shuffleMethod) {
		case "cut":
			deck=shuffleCut(deck,shuffle, arg1);
			break;
		case "deals with":
			deck=shuffleDealsWithIncrement(deck,shuffle, Integer.valueOf(StringUtils.split(shuffle, " ")[3]));
			break;
		case "deals into":
			deck=shuffleDealsInto(deck,shuffle);
			break;

		default:
			break;
		}
		}
		return deck;
		
	}


	private List<Integer> shuffleDealsInto(List<Integer> deck, String shuffle) {
		List<Integer> newDeck = new ArrayList<Integer>();
		for (int i=0;i<deck.size();i++) {
			newDeck.add(i, deck.get(deck.size()-1-i));
		}
		return newDeck;
	}


	private List<Integer> shuffleDealsWithIncrement(List<Integer> deck, String shuffle, Integer increment) {
		List<Integer> newDeck = new ArrayList<Integer>();
		
		//init newDeck
		for (int i=0;i<deck.size();i++) {
			newDeck.add(null);
		}
		
		//shuffle
		for (int i=0;i<deck.size();i++) {
			newDeck.set((i*increment)%deck.size(), deck.get(i));
		}
		
		return newDeck;
	}


	private List<Integer> shuffleCut(List<Integer> deck, String shuffle, String arg1) {
		List<Integer> newDeck = new ArrayList<Integer>();
		
		if (Integer.valueOf(arg1)>0) {

			for (int i=0;i<deck.size()-Integer.valueOf(arg1);i++) {
				newDeck.add(i,deck.get(i+Integer.valueOf(arg1)));
			}
			for (int j=0;j<Integer.valueOf(arg1);j++) {
				newDeck.add(deck.get(j));
			}
		}
		
		if (Integer.valueOf(arg1)<0) {
			newDeck=shuffleCut(deck,shuffle,""+(deck.size()+Integer.valueOf(arg1)));
	
		}
		
		
		return newDeck;
	}


	public void creationDeck(List<Integer> deck, long factoryOrder) {
		for (int i=0;i<factoryOrder;i++) {
			deck.add(i);
		}
	}
	
	
	// run2 ----
	public void run2() throws IOException {
		List<String> input = lectureInput();		
		List<Integer> deck = new ArrayList<Integer>();
		long factoryOrder=119315717514047L;
		creationDeck(deck, factoryOrder);
		deck=shuffling(deck, input);
		int indice=rechercheIndiceCarte(deck,2019);
		
		
		System.out.println(indice);
		
		
		

	}
	
	
	private List<String> lectureInput() {
		String filename = "src/main/resources/day01.txt";
		return Outil.importationString(filename);
	}
}
