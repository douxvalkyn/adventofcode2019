package outils;

import java.util.ArrayList;
import java.util.List;

public class Case {

	
	private int x;
	private int y;
	private int rang = 0;
	private int valeur;
	private String etat;

	
	public Case(int x, int y) {
		super();
		this.x = x;
		this.y = y;
		this.valeur=0;
		this.etat="_";
		
	}
	
	
	public int getValeur() {
		return valeur;
	}

	public void setValeur(int valeur) {
		this.valeur = valeur;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getRang() {
		return rang;
	}

	public void setRang(int rang) {
		this.rang = rang;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtatPlusUn() {
		this.valeur=this.valeur+1;
	}
	@Override
	public String toString() {
		return "Case"+rang+ "[x=" + x + ", y=" + y+"] (" + valeur +")" + "("+etat+")";
	}
	
	
	//adjacence Rook
	public List<Case> getCasesAdjacentes(Grille grille){
		x=this.getX();
		y=this.getY();
		Case nord = grille.getCase(x, y-1);
		Case sud = grille.getCase(x, y+1);
		Case est = grille.getCase(x+1, y);
		Case ouest = grille.getCase(x-1, y);
		List<Case> casesAdj= new ArrayList<Case>();
		if (nord!= null) {
			casesAdj.add(nord);
		}
		if (sud!= null) {
			casesAdj.add(sud);
			}
		if (est!= null) {
			casesAdj.add(est);
			}
		if (ouest!= null) {
			casesAdj.add(ouest);
			}
		return casesAdj;
	}
	
	
	//adjacence Queen
	public List<Case> getCasesAdjacentesQueen(Grille grille){
		x=this.getX();
		y=this.getY();
		Case nn = grille.getCase(x, y-1);
		Case ss = grille.getCase(x, y+1);
		Case ee = grille.getCase(x+1, y);
		Case oo = grille.getCase(x-1, y);
		Case no = grille.getCase(x-1, y-1);
		Case ne = grille.getCase(x+1, y-1);
		Case so = grille.getCase(x-1, y+1);
		Case se = grille.getCase(x+1, y+1);
		List<Case> casesAdj= new ArrayList<Case>();
		if (nn!= null) {
			casesAdj.add(nn);
		}
		if (ss!= null) {
			casesAdj.add(ss);
			}
		if (ee!= null) {
			casesAdj.add(ee);
			}
		if (oo!= null) {
			casesAdj.add(oo);
			}
		if (no!= null) {
			casesAdj.add(no);
		}
		if (ne!= null) {
			casesAdj.add(ne);
			}
		if (so!= null) {
			casesAdj.add(so);
			}
		if (se!= null) {
			casesAdj.add(se);
			}
		return casesAdj;
	}
	
}
