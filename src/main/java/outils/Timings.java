package outils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import day22.Day22;

public class Timings {
	
	private static final String ANSI_RESET="\u001B[0m";
	private static final String ANSI_RED="\u001B[31m";
	private static final String ANSI_GREEN="\u001B[32m";
	private static final String ANSI_BRIGHT_GREEN="\u001B[92m";
	private static final String ANSI_YELLOW="\u001B[33m";
	private static final String ANSI_PURPLE="\u001B[35m";
	private static final String ANSI_BLUE="\u001B[34m";
	private static final String ANSI_BRIGHT_MAGENTA="\u001B[95m";


	public static void main(String[] args) throws IOException {
		List<Long> dureesRun1 = new ArrayList<Long>();
		List<Long> dureesRun2 = new ArrayList<Long>();

		//Day01
		System.out.println("----- DAY 01 -----");
		long start = System.currentTimeMillis();
		Day22 day01 = new Day22();
		day01.run1();
		long duree = System.currentTimeMillis() - start;		
		dureesRun1.add(duree);
		start = System.currentTimeMillis();
		day01.run2();
		long duree2 = System.currentTimeMillis() - start;
		dureesRun2.add(duree2);
		
		

		System.out.println("-------------------------------------------------------");
		System.out.println("------------------"+ANSI_BLUE+"Advent of Code 2021"+ANSI_RESET+"------------------");
		System.out.println("------------------------"+ANSI_BLUE+"Timings"+ANSI_RESET+"------------------------");
		System.out.println("-------------------------------------------------------");
		for (int i=0; i<dureesRun1.size();i++) {
			long calcul1 = dureesRun1.get(i);
			String calculString1 = ""+calcul1;
			long calcul2 = dureesRun2.get(i);
			String calculString2 = ""+calcul2;
			if (i<9) {
			System.out.println("[ Day"+(i+1)+" -  Run1: " + StringUtils.leftPad(colored(calculString1), 18, ".") + " ms - Run2: " + StringUtils.leftPad(colored(calculString2), 18, ".") + " ms]]");
			}
			if (i>=9) {
			System.out.println("[ Day"+(i+1)+" - Run1: " + StringUtils.leftPad(colored(calculString1), 18, ".") + " ms - Run2: " + StringUtils.leftPad(colored(calculString2), 18, ".") + " ms]]");
			}
		}
		long sum = 0;
		for (long dur : dureesRun1) {
			sum=sum+dur;
		}
		for (long dur : dureesRun2) {
			sum=sum+dur;
		}
		long sumSecondes=sum/1000;
		System.out.println("[ Total:      " + sum + " ms soit "+ sumSecondes +" s]");
	}


	private static String colored(String calculString) {
	String res = "";
		if (Integer.parseInt(calculString)<1000) {
			res=ANSI_BRIGHT_GREEN+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>=1000 & Integer.parseInt(calculString)<10000) {
			res=ANSI_GREEN+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>=10000 & Integer.parseInt(calculString)<100000) {
			res=ANSI_YELLOW+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>=100000 & Integer.parseInt(calculString)<1000000) {
			res=ANSI_RED+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>=1000000 & Integer.parseInt(calculString)<10000000) {
			res=ANSI_PURPLE+calculString+ANSI_RESET;
		}
		if (Integer.parseInt(calculString)>1000000) {
			res=ANSI_BRIGHT_MAGENTA+calculString+ANSI_RESET;
		}
		return res;
	}

}
