################################
#-------- 25 Decembre ---------#
################################
source("script.R")

data <- unlist(read_csv("input/input25.txt",  col_names = FALSE))


programme_intcode8=function(data, inputs,indice_i,rb){
  #agrandir le data en mettant des zéros au bout
  # data <- c(data,rep(0,1000))
  i=indice_i
  GO=TRUE
  STOP="FEU VERT"
  relative_base=rb
  outputs=c()
  inputs=inputs
  while(GO){
    opcode=data[i]
    
    action=str_sub(opcode,nchar(opcode)-1,nchar(opcode))
    if (nchar(opcode)>2){mode_param1=str_sub(opcode,nchar(opcode)-2,nchar(opcode)-2) }else{mode_param1='0'}
    if (nchar(opcode)>3){mode_param2=str_sub(opcode,nchar(opcode)-3,nchar(opcode)-3) }else{mode_param2='0'}
    if (nchar(opcode)>4){mode_param3=str_sub(opcode,nchar(opcode)-4,nchar(opcode)-4) }else{mode_param3='0'}
    
    if (action %in% c('1','01')){ #addition
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (mode_param3==0){data[data[i+3]+1] <- param1+param2}
      if (mode_param3==1){data[data[i+3]+1] <- param1+param2}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- param1+param2 }
      pas=4
    }
    if (action %in% c('2','02')){ #multiplication
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (mode_param3==0){data[data[i+3]+1] <- param1*param2}
      if (mode_param3==1){data[data[i+3]+1] <- param1*param2}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- param1*param2 }
      pas=4
    }
    
    if (action %in% c('3','03')){ #input
      if (mode_param1==0){param1=data[i+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=relative_base+data[i+1]}
      if (length(inputs)==0){print("input manquant !!!")}
      data[param1+1] <- inputs[1]; #on utilise le premier input de la reserve
      pas=2
      # on supprime l'input utilisé,on conserve ceux qui restent en stock
      inputs= inputs[-1]
    }
    
    if (action %in% c('4','04')){ #output
      if (mode_param1==0){param1=data[data[i+1]+1] }
      if (mode_param1==1){param1=data[i+1] }
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      # print(param1)
      outputs=c(outputs,param1)
      pas=2
      #GO=FALSE    
    }
    
    if (action %in% c('5','05')){ #jump si != 0
      if (mode_param1==0){param1=data[data[i+1]+1] }
      if (mode_param1==1){param1=data[i+1] }
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1 != 0){ pas=-i+param2+1}else{pas=3}
    }
    
    if (action %in% c('6','06')){ #jump si == 0
      if (mode_param1==0){param1=data[data[i+1]+1] }
      if (mode_param1==1){param1=data[i+1] }
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1 == 0){ pas=-i+param2+1}else{pas=3}
    }
    
    if (action %in% c('7','07')){ #test inf
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1<param2){ res=1}else{res=0}
      if (mode_param3==0){data[data[i+3]+1] <- res}
      if (mode_param3==1){data[data[i+3]+1] <- res}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- res}
      pas=4
    }
    
    if (action %in% c('8','08')){ #test egalité
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1==param2){ res=1}else{res=0}
      if (mode_param3==0){data[data[i+3]+1] <- res}
      if (mode_param3==1){data[data[i+3]+1] <- res}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- res}
      pas=4
    }
    
    if (action %in% c('9','09')){ # modif relative base
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      relative_base <-relative_base+param1
      pas=2
    }
    
    if (action==99){
      GO=FALSE
      STOP="FEU ROUGE" #met l'amplifier en arret
    }
    
    i=i+pas
    if (i<0){GO <-FALSE}else{
      if (length(inputs)==0 & str_sub(data[i],nchar(data[i])-1,nchar(data[i]))%in% c('3','03')){GO=FALSE;}
      if (length(inputs)>0){ if (all(is.na(inputs)) & str_sub(data[i],nchar(data[i])-1,nchar(data[i]))%in% c('3','03')){GO=FALSE}
      } #met l'amplifier en pause si l'input est vide et qu'il va être requis
    }
  }
  
  #on sauvegarde l'output, le data, et le i pour savoir ou on est rendu; + inputs pour savoir ceux qui restent à utiliser
  #ajout d'un voyant lumineux STOP qui indique que l'amplifier cesse de fonctionner
  sortie = list(outputs,data,i,inputs, STOP,relative_base)
  return(sortie)
}


#### Exploration du vaiseau ----
res=programme_intcode8(data,NA,1,0)
intToUtf8(res[[1]])
commande_N<-c(utf8ToInt('north'),10)
commande_E<-c(utf8ToInt('east'),10)
commande_W<-c(utf8ToInt('west'),10)
commande_S<-c(utf8ToInt('south'),10)

commande_take_coin<-c(utf8ToInt('take coin'),10)
commande_drop_coin<-c(utf8ToInt('drop coin'),10)
commande_take_hypercube<-c(utf8ToInt('take hypercube'),10)
commande_drop_hypercube<-c(utf8ToInt('drop hypercube'),10)
commande_take_shell<-c(utf8ToInt('take shell'),10)
commande_drop_shell<-c(utf8ToInt('drop shell'),10)
commande_take_astrolabe<-c(utf8ToInt('take astrolabe'),10)
commande_drop_astrolabe<-c(utf8ToInt('drop astrolabe'),10)
commande_take_klein_bottle<-c(utf8ToInt('take klein bottle'),10)
commande_drop_klein_bottle<-c(utf8ToInt('drop klein bottle'),10)
commande_take_easter_egg<-c(utf8ToInt('take easter egg'),10)
commande_drop_easter_egg<-c(utf8ToInt('drop easter egg'),10)
# commande_take_photons<-c(utf8ToInt('take photons'),10)
commande_take_tambourine<-c(utf8ToInt('take tambourine'),10)
commande_drop_tambourine<-c(utf8ToInt('drop tambourine'),10)
commande_take_dark_matter<-c(utf8ToInt('take dark matter'),10)
commande_drop_dark_matter<-c(utf8ToInt('drop dark matter'),10)
# commande_take_giant_electromagnet<-c(utf8ToInt('take giant electromagnet'),10)
# commande_take_molten_lava<-c(utf8ToInt('take molten lava'),10)
# commande_take_escape_pod<-c(utf8ToInt('take escape pod'),10)
# commande_take_infinite_loop<-c(utf8ToInt('take infinite loop'),10)
commande_inv<-c(utf8ToInt('inv'),10)
res=programme_intcode8(res[[2]], commande_inv,res[[3]], res[[6]])
my_output=intToUtf8(res[[1]])
write.table(my_output, file="data.txt", sep = "\t")

#tests de charge 8 elements max:  1 à 6 elements
res2=programme_intcode8(res[[2]], commande_inv,res[[3]], res[[6]])
res2=programme_intcode8(res2[[2]], commande_inv,res2[[3]], res2[[6]])
res2=programme_intcode8(res2[[2]], commande_S,res2[[3]], res2[[6]])
my_output=intToUtf8(res2[[1]])
write.table(my_output, file="data.txt", sep = "\t")




items=c("coin","hypercube", "shell", "astrolabe","klein_bottle", "easter_egg", "tambourine", "dark_matter"  )

library(tidyr)
library(rlang)
res_vide #aucun item transporté
ejected=utf8ToInt('\n\n\n== Pressure-Sensitive Floor ==\nAnalyzing...\n\nDoors here lead:\n- north\n\nA loud, robotic voice says \"Alert! Droids on this ship are')


# Test si 1 seul item porté
for (i in 1:8){
  item=items[i]
  commande=paste0("commande_take_", item)
  res2=programme_intcode8(res_vide[[2]],eval_tidy(parse_expr(commande)) ,res_vide[[3]], res_vide[[6]])
  res2=programme_intcode8(res2[[2]], commande_S,res2[[3]], res2[[6]])
  my_output=intToUtf8(res2[[1]])
  rep=res2[[1]]

  if (any(rep[1:132] !=ejected)){print(item)}
}


# Test si 2 items portés
t0=Sys.time()
for (i in 1:8){
  t1=Sys.time()
  print(i);print(t1-t0)
  item=items[i]
  commande1=paste0("commande_take_", item)
  items2= items[-i]
  for (j in 1:7){
  item=items2[j]
  commande2=paste0("commande_take_", item)
  
  res2=programme_intcode8(res_vide[[2]],eval_tidy(parse_expr(commande1)) ,res_vide[[3]], res_vide[[6]])
  res2=programme_intcode8(res2[[2]],eval_tidy(parse_expr(commande2)) ,res2[[3]], res2[[6]])
  res2=programme_intcode8(res2[[2]], commande_S,res2[[3]], res2[[6]])
  my_output=intToUtf8(res2[[1]])
  rep=res2[[1]]
  
  if (any(rep[1:132] !=ejected)){print("TROUVE");print(c(i,j)); write.table(my_output, file="data.txt", sep = "\t")}
  }
}

t0=Sys.time()
# Test si 3 items portés
for (i in 1:8){
  t1=Sys.time()
  print(i);print(t1-t0)
  item=items[i]
  commande1=paste0("commande_take_", item)
  items2= items[-i]
  for (j in 1:7){
    item=items2[j]
    commande2=paste0("commande_take_", item)
    items3= items2[-j]
    for (k in 1:6){
      item=items3[k]
      commande3=paste0("commande_take_", item)
      items4= items3[-k]
      
      res2=programme_intcode8(res_vide[[2]],eval_tidy(parse_expr(commande1)) ,res_vide[[3]], res_vide[[6]])
      res2=programme_intcode8(res2[[2]],eval_tidy(parse_expr(commande2)) ,res2[[3]], res2[[6]])
      res2=programme_intcode8(res2[[2]],eval_tidy(parse_expr(commande3)) ,res2[[3]], res2[[6]])
      res2=programme_intcode8(res2[[2]], commande_S,res2[[3]], res2[[6]])
      my_output=intToUtf8(res2[[1]])
      rep=res2[[1]]
      
      if (any(rep[1:132] !=ejected)){print("TROUVE");print(c(i,j,k))write.table(my_output, file="data.txt", sep = "\t")}

    }
  }
}


t0=Sys.time()
# Test si 4 items portés
for (i in 1:8){
  t1=Sys.time()
  print('i');print(i);print(t1-t0)
  item=items[i]
  commande1=paste0("commande_take_", item)
  items2= items[-i]
  for (j in 1:7){
    print('j');print(j)
    item=items2[j]
    commande2=paste0("commande_take_", item)
    items3= items2[-j]
    for (k in 1:6){
      item=items3[k]
      commande3=paste0("commande_take_", item)
      items4= items3[-k]
      for (l in 1:5){
        item=items4[l]
        commande4=paste0("commande_take_", item)
        items5= items4[-l]
        
        res2=programme_intcode8(res_vide[[2]],eval_tidy(parse_expr(commande1)) ,res_vide[[3]], res_vide[[6]])
        res2=programme_intcode8(res2[[2]],eval_tidy(parse_expr(commande2)) ,res2[[3]], res2[[6]])
        res2=programme_intcode8(res2[[2]],eval_tidy(parse_expr(commande3)) ,res2[[3]], res2[[6]])
        res2=programme_intcode8(res2[[2]],eval_tidy(parse_expr(commande4)) ,res2[[3]], res2[[6]])
        res2=programme_intcode8(res2[[2]], commande_S,res2[[3]], res2[[6]])
        my_output=intToUtf8(res2[[1]])
        rep=res2[[1]]
        
        if (any(rep[1:132] !=ejected)){print("TROUVE");print(c(i,j,k,l));print(items5);
          write.table(my_output, file="data.txt", sep = "\t")}
        
      }
    }
  }
}
i=1
j=1
k=5
l=5