install.packages("readr")
install.packages("stringr")
install.packages("dplyr")
install.packages("igraph")

library(readr)
library(stringr)
library(dplyr)
library(igraph)
