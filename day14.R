################################
#-------- 14 Decembre ---------#
################################
source("script.R")
options(digits = 20)

#### Import input ----

data <- read_delim("input/input14f.txt", ";",   escape_double = FALSE, col_names = FALSE, trim_ws = TRUE)

imp=function(data){
data2=data.frame()
for (i in 1:dim(data)[1]){
data2 <-rbind(data2, str_split_fixed(data[i,],' => ',n=Inf))
}
data2$V1<- as.character((data2$V1))
data3 <- cbind(data2[,1],str_split_fixed(data2$V2," ", n=Inf))
composants <- data3[,3]
composants2 <-list()
for (i in 1:length(composants)){
  composants2[[length(composants2)+1]] <- str_split(unlist(str_split(data3[i,1],", "))," ")
}

return(list(data3,composants,composants2))
}

data3<-imp(data)[[1]]
composants<-imp(data)[[2]]
composants2<-imp(data)[[3]]




#### Recherche des composants du FUEL ----
fuel <-composants2[[which(composants=="FUEL")]]

echange_sans_perte = function(fuel){
#on regarde si on peut échanger sans perte l'un des composants

for (j in 1:length(fuel)){
  nombre<-as.numeric(fuel[[j]][1])
  nom<-fuel[[j]][2]
  #possible si nb est un multiple de paquet OU SI NOMBRE > PAQUET, IL Y AURA DU RESTE !
  if (nom=="ORE"){paquet<-Inf}else{  paquet <- as.numeric(data3[,2][which(data3[,3]==nom)])}
  if (nombre >= paquet){
    remplacants <- composants2[[ which(composants==nom)]] #liste des composants à remplacer
    a_ajouter=c()
    reste=c()
    
    for (k in 1:length(remplacants)){
      remplacant<-remplacants[[k]]
      nombre_remplacant <-as.numeric(remplacant[1])
      mot_remplacant <-remplacant[2]
      a_ajouter<-c(a_ajouter,nombre_remplacant*floor(nombre/paquet),mot_remplacant)
    }
    if (nombre - paquet* floor(nombre/paquet)>0){ reste <- c(reste,nombre - paquet* floor(nombre/paquet),nom) }
    fuel[[j]] <- a_ajouter
    fuel[[length(fuel)+1]]<- reste
  }
} 
  return(fuel)
}



remise_ordre_fuel = function (fuel){
#remise en ordre de fuel
fuel2<-fuel
indices_a_supprimer=c()
for (j in 1:length(fuel)){
  if (length(fuel[[j]])>2){
    k=1
    while (k <= (length(fuel[[j]]))-1) {
      fuel2[[length(fuel2)+1]] <-c(fuel[[j]][k],fuel[[j]][k+1])
      k=k+2
    }
    indices_a_supprimer = c(indices_a_supprimer,j)
  }
}
if (length(indices_a_supprimer)>0){
  for (l in 1:length(indices_a_supprimer)){  fuel2[[indices_a_supprimer[l]]]<- NA;}
}
fuel2 <-fuel2[!is.na(fuel2)]
fuel<-fuel2 ; rm(fuel2);
return(fuel)
}

fuel <- remise_ordre_fuel(fuel)


somme_composants_identiques=function(fuel){
#somme des composants identiques dans fuel
df <- as.data.frame(do.call("rbind", fuel))
df<-data.frame(lapply(df, as.character), stringsAsFactors=FALSE)
df$V1 <- as.numeric(df$V1)
df<-df %>%  group_by(df$V2) %>% summarise(s=sum(V1))
fuel<- list()
for (i in 1:dim(df)[1]){
  fuel[[length(fuel)+1]] <- c(as.character(df[i,2]),as.character(df[i,1]))
}
return(fuel)
}

fuel <- somme_composants_identiques(fuel)


#on échange en minimisant le cout: essai en échangeant uniquement les composants de profondeur maximale

echange_avec_perte=function(fuel){
#recherche des composants à profondeur max
p=c()
for (j in 1:length(fuel)){
  nombre<-as.numeric(fuel[[j]][1])
  nom<-fuel[[j]][2]
  if (nom=="ORE"){p[j]=0}else{
    p[j]=profondeur(nom)}
}
#on fait les échanges pour les composants de profondeur max
 indices_a_echanger <- which(p==max(p))
 for (j in indices_a_echanger){
    nombre<-as.numeric(fuel[[j]][1])
    nom<-fuel[[j]][2]
    paquet <- as.numeric(data3[,2][which(data3[,3]==nom)])
    surplus[[length(surplus)+1]] <<- c(paquet - nombre,nom )
    remplacants <- composants2[[ which(composants==nom)]]
    a_ajouter=c()
    excedent=c()
    for (k in 1:length(remplacants)){
      remplacant<-remplacants[[k]]
      nombre_remplacant <-as.numeric(remplacant[1])
      mot_remplacant <-remplacant[2]
      a_ajouter<-c(a_ajouter,nombre_remplacant,mot_remplacant)
      excedent <- c(excedent, paquet -  nombre,nom)
    }
    fuel[[j]] <- a_ajouter
  }
return(fuel)
}


#calcul du niveau de profondeur d'un composant
profondeur=function(nom){
  prof<-0
  ind=which(data3[,3]==nom)
  ingredients<- (str_split(unlist(str_split(data3[ind,1],', ')),' '))
  ingredients2<- c()
  for (i in 1:length(ingredients)){ingredients2<-c(ingredients2,ingredients[[i]][2])}
  if (length(ingredients2)==1){
    if (ingredients2=="ORE"){prof=1}else{prof=1+profondeur(ingredients2)}
  }
  if (length(ingredients2)>=2){
    res=c()
    for (j in 1:length(ingredients2)){
      res[j] <- profondeur(ingredients2[j])
    }
   prof=1+max(res) 
  }

  return(prof) 
}




#### Etapes ----
fuel <-composants2[[which(composants=="FUEL")]]
surplus <-list()
fuel <- echange_sans_perte(fuel)
fuel <- remise_ordre_fuel(fuel)
fuel <- somme_composants_identiques(fuel)

fuel <- echange_avec_perte(fuel)
fuel <- remise_ordre_fuel(fuel)
fuel <- somme_composants_identiques(fuel)



#### Part 2 ----
# 
# The 13312 ORE-per-FUEL example could produce 82892753 FUEL.
# The 180697 ORE-per-FUEL example could produce 5586022 FUEL.
# The 2210736 ORE-per-FUEL example could produce 460664 FUEL.

#on étudie le surplus dès qu'il est complet
a_ajouter=list()

for (j in 1:length(surplus)){
  if (surplus[[j]][2] != "ORE"){
  indice<-which(data3[,3]==surplus[[j]][2])
  
  for (i in 1:length(unlist(composants2[indice]))){
    if (i%%2!=0){
      x <- as.numeric(unlist(composants2[indice])[i] ) * as.numeric(surplus[[j]][1]) / as.numeric(data3[,2][indice]) 
      y <-  unlist(composants2[indice])[i+1]
      a_ajouter[[length(a_ajouter)+1]] <- c(x,y)
    }
  }
  }
  if (surplus[[j]][2] == "ORE"){
    a_ajouter[[length(a_ajouter)+1]] <- c(surplus[[j]][1],"ORE")
  }
}

surplus <- remise_ordre_fuel(a_ajouter)

surplus <- somme_composants_identiques((surplus))

1000000000000/(as.numeric(fuel[[1]][1])-as.numeric(surplus[[1]][1]))

# 1376632 too high
# 1376631 right (faire - 1, histoire d'arrondis ?)