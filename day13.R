################################
#-------- 13 Decembre ---------#
################################
source("script.R")

data <- unlist(read_csv("input/input13.txt",  col_names = FALSE))   


programme_intcode7=function(data, inputs,indice_i,rb){
  #agrandir le data en mettant des zéros au bout
  data <- c(data,rep(0,300))
  i=indice_i
  GO=TRUE
  STOP="FEU VERT"
  relative_base=rb
  outputs=c()
  inputs=inputs
  while(GO){
    opcode=data[i]
  
    action=str_sub(opcode,nchar(opcode)-1,nchar(opcode))
    if (nchar(opcode)>2){mode_param1=str_sub(opcode,nchar(opcode)-2,nchar(opcode)-2) }else{mode_param1='0'}
    if (nchar(opcode)>3){mode_param2=str_sub(opcode,nchar(opcode)-3,nchar(opcode)-3) }else{mode_param2='0'}
    if (nchar(opcode)>4){mode_param3=str_sub(opcode,nchar(opcode)-4,nchar(opcode)-4) }else{mode_param3='0'}
    
    if (action %in% c('1','01')){ #addition
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (mode_param3==0){data[data[i+3]+1] <- param1+param2}
      if (mode_param3==1){data[data[i+3]+1] <- param1+param2}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- param1+param2 }
      pas=4
    }
    if (action %in% c('2','02')){ #multiplication
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (mode_param3==0){data[data[i+3]+1] <- param1*param2}
      if (mode_param3==1){data[data[i+3]+1] <- param1*param2}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- param1*param2 }
      pas=4
    }
    
    if (action %in% c('3','03')){ #input
      if (mode_param1==0){param1=data[i+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=relative_base+data[i+1]}
      if (length(inputs)==0){print("input manquant !!!")}
      data[param1+1] <- inputs[1]; #on utilise le premier input de la reserve
      pas=2
      # on supprime l'input utilisé,on conserve ceux qui restent en stock
      inputs= inputs[-1]
    }
    
    if (action %in% c('4','04')){ #output
      if (mode_param1==0){param1=data[data[i+1]+1] }
      if (mode_param1==1){param1=data[i+1] }
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      # print(param1)
      outputs=c(outputs,param1)
      pas=2
      #GO=FALSE    
    }
    
    if (action %in% c('5','05')){ #jump si != 0
      if (mode_param1==0){param1=data[data[i+1]+1] }
      if (mode_param1==1){param1=data[i+1] }
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1 != 0){ pas=-i+param2+1}else{pas=3}
    }
    
    if (action %in% c('6','06')){ #jump si == 0
      if (mode_param1==0){param1=data[data[i+1]+1] }
      if (mode_param1==1){param1=data[i+1] }
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1 == 0){ pas=-i+param2+1}else{pas=3}
    }
    
    if (action %in% c('7','07')){ #test inf
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1<param2){ res=1}else{res=0}
      if (mode_param3==0){data[data[i+3]+1] <- res}
      if (mode_param3==1){data[data[i+3]+1] <- res}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- res}
      pas=4
    }
    
    if (action %in% c('8','08')){ #test egalité
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1==param2){ res=1}else{res=0}
      if (mode_param3==0){data[data[i+3]+1] <- res}
      if (mode_param3==1){data[data[i+3]+1] <- res}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- res}
      pas=4
    }
    
    if (action %in% c('9','09')){ # modif relative base
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      relative_base <-relative_base+param1
      pas=2
    }
    
    if (action==99){
      GO=FALSE
      STOP="FEU ROUGE" #met l'amplifier en arret
    }
    
    i=i+pas
    if (length(inputs)==0 & str_sub(data[i],nchar(data[i])-1,nchar(data[i]))%in% c('3','03')){GO=FALSE}
    if (length(inputs)>0){ if (is.na(inputs) & str_sub(data[i],nchar(data[i])-1,nchar(data[i]))%in% c('3','03')){GO=FALSE}   #met l'amplifier en pause si l'input est vide et qu'il va être requis
    }
  }
  
  #on sauvegarde l'output, le data, et le i pour savoir ou on est rendu; + inputs pour savoir ceux qui restent à utiliser
  #ajout d'un voyant lumineux STOP qui indique que l'amplifier cesse de fonctionner
  sortie = list(outputs,data,i,inputs, STOP,relative_base)
  return(sortie)
}



## Création du jeu ----

grille= matrix('.',nrow=25,ncol=40)

instructions <- programme_intcode7(data,NA ,1,0)[[1]]
dessine_grille=function(instructions, grille){
  cpt=0;
  imax=length(instructions) -2
  for(i in seq(1, imax, 3)){
    x<-instructions[i]
    y<-instructions[i+1]
    id<-instructions[i+2]
    i<- i+3
    if (id==0){grille[y+1,x+1]<-NA} #Empty
    if (id==1){grille[y+1,x+1]<-'####'} #WALL indestructible
    if (id==2){grille[y+1,x+1]<-'XXXX';cpt=cpt+1} #BLOCK destructible
    if (id==3){grille[y+1,x+1]<-'---@'} #PADDLE Horizontal
    if (id==4){grille[y+1,x+1]<-'O'} #BALL
    
  }
  return(grille)
}
grille<-dessine_grille(instructions,grille)




#initialisation
paddle_x = 19
data[1]<-2
test=programme_intcode7(data, NA,1,0)
tests=list()
n=0;a=0;b=0;score=c();
g=grille


#boucle
while (length(tests)<9800){
GO=TRUE
i=length(tests)
etape<-i
while (GO){
  #menage dans les vieux datasets
  if(i>50){g<-dessine_grille(tests[[i-50]][[1]], g);tests[[i-50]]<- 0}
  
  i=i+1
  balle_y <-last(test[[1]][which(test[[1]]==4)-1]) # hauteur balle à étape d'avant
  test=programme_intcode7(test[[2]], 0,test[[3]],test[[6]]) #pas bouger joystick
  #Lecture du score
  if (any((test[[1]])==-1)){score<- c(score,test[[1]][which(test[[1]]==-1)+2])}
  # print((test[[1]]))
  new_balle_y <- last(test[[1]][which(test[[1]]==4)-1]) #hauteur balle apres nouvelle étape
  if ((new_balle_y<balle_y) & (last(test[[1]][which(test[[1]]==4)-1])==17)) {etape<-i} #à y=17, on voit la balle remonter car elle s'arrete en y=18 puisque le paddle est en y=19
  test[[7]]<-0
  if (any((test[[1]])==-1) & last(test[[1]][which(test[[1]]==-1)+2])!=0){test[[7]]<-sum((test[[1]])==-1)} #compter le nb de blocks touchés
  tests[[i]]<- test #conserver l'historique des mouvements
  #stop quand la balle est tombée
  if (any((test[[1]])==-1) & last(test[[1]][which(test[[1]]==-1)+2])==0){GO=FALSE}
}



#position de la balle à attraper (là où le paddle doit aller pour l'intercepter)
tests_prec <- unlist(tests[[length(tests)-1]][1])
xmax <- length(tests_prec)-1
for (x in 1:xmax){
 if( tests_prec[x]==19 & tests_prec[x+1]==4 ){balle_x <- as.numeric(tests_prec[x-1])}
}


#ecart entre balle et paddle
ecart<- abs(paddle_x - balle_x)

#on relance le jeu et on déplace le paddle du nb de fois = ecart, dans la direction voulue pour attraper la balle
#on repart depuis ETAPE-1 (le dernier moment ou le paddle a touché la balle) ou depuis une étape plus proche
if (length(tests)-etape>50){etape<-length(tests)-ecart-5}
test=tests[[etape]]

if (paddle_x < balle_x){sens<- 1}else{sens <- -1}
etape2<-etape
for (j in 1:(ecart)){
  # +1 on deplace à droite
  # -1 on deplace à gauche
  test=programme_intcode7(test[[2]], sens,test[[3]],test[[6]])
  #Lecture du score
  if (any((test[[1]])==-1)){score<- c(score,test[[1]][which(test[[1]]==-1)+2])}
  test[[7]]<-0
  if (any((test[[1]])==-1) & last(test[[1]][which(test[[1]]==-1)+2])!=0){test[[7]]<-sum((test[[1]])==-1)} #compter le nb de blocks touchés
  tests[[etape2+j]]<- test
  # print(unlist(test[1]))
  # print('---')
  
}

#nouvel emplacement du paddle
paddle_x = balle_x

#comptage blocks touchés
# cpt=0;for (iter in 1:length(tests)){cpt=cpt+tests[[iter]][[7]]}

#score max atteint
print(paste("Score: ",max(score)))

# !! en cas de répétition, déplacer le paddle 1 cran de plus !!
  n=n+1
  if (n%%4==0){a <-paddle_x }
  if (n%%4==2){b <-paddle_x }
  if ((a==b & length(tests)>100) | max(score)%in% c(10568,10720) | (max(score)==10832 & length(tests)%%3==0)){
  test=programme_intcode7(test[[2]], sens,test[[3]],test[[6]])
  #Lecture du score
  if (any((test[[1]])==-1)){score<- c(score,test[which(test==-1)+2])}
  test[[7]]<-0
  if (any((test[[1]])==-1) & last(test[[1]][which(test[[1]]==-1)+2])!=0){test[[7]]<-sum((test[[1]])==-1)}
  tests[[length(tests)+1]]<- test
  paddle_x = paddle_x+sens
  }

}

#10802 non
#10832 non
#10776: le prog plante quand cette ligne apparait car la balle disparait !:
# 2     7     0    -1     0 10776

