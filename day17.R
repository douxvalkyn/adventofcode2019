################################
#-------- 17 Decembre ---------#
################################
source("script.R")

data <- unlist(read_csv("input/input17.txt",  col_names = FALSE))  


programme_intcode7=function(data, inputs,indice_i,rb){
  #agrandir le data en mettant des zéros au bout
  # data <- c(data,rep(0,1000))
  i=indice_i
  GO=TRUE
  STOP="FEU VERT"
  relative_base=rb
  outputs=c()
  inputs=inputs
  while(GO){
    opcode=data[i]
    
    action=str_sub(opcode,nchar(opcode)-1,nchar(opcode))
    if (nchar(opcode)>2){mode_param1=str_sub(opcode,nchar(opcode)-2,nchar(opcode)-2) }else{mode_param1='0'}
    if (nchar(opcode)>3){mode_param2=str_sub(opcode,nchar(opcode)-3,nchar(opcode)-3) }else{mode_param2='0'}
    if (nchar(opcode)>4){mode_param3=str_sub(opcode,nchar(opcode)-4,nchar(opcode)-4) }else{mode_param3='0'}
    
    if (action %in% c('1','01')){ #addition
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (mode_param3==0){data[data[i+3]+1] <- param1+param2}
      if (mode_param3==1){data[data[i+3]+1] <- param1+param2}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- param1+param2 }
      pas=4
    }
    if (action %in% c('2','02')){ #multiplication
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (mode_param3==0){data[data[i+3]+1] <- param1*param2}
      if (mode_param3==1){data[data[i+3]+1] <- param1*param2}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- param1*param2 }
      pas=4
    }
    
    if (action %in% c('3','03')){ #input
      if (mode_param1==0){param1=data[i+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=relative_base+data[i+1]}
      if (length(inputs)==0){print("input manquant !!!")}
      data[param1+1] <- inputs[1]; #on utilise le premier input de la reserve
      pas=2
      # on supprime l'input utilisé,on conserve ceux qui restent en stock
      inputs= inputs[-1]
    }
    
    if (action %in% c('4','04')){ #output
      if (mode_param1==0){param1=data[data[i+1]+1] }
      if (mode_param1==1){param1=data[i+1] }
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      # print(param1)
      outputs=c(outputs,param1)
      pas=2
      #GO=FALSE    
    }
    
    if (action %in% c('5','05')){ #jump si != 0
      if (mode_param1==0){param1=data[data[i+1]+1] }
      if (mode_param1==1){param1=data[i+1] }
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1 != 0){ pas=-i+param2+1}else{pas=3}
    }
    
    if (action %in% c('6','06')){ #jump si == 0
      if (mode_param1==0){param1=data[data[i+1]+1] }
      if (mode_param1==1){param1=data[i+1] }
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1 == 0){ pas=-i+param2+1}else{pas=3}
    }
    
    if (action %in% c('7','07')){ #test inf
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1<param2){ res=1}else{res=0}
      if (mode_param3==0){data[data[i+3]+1] <- res}
      if (mode_param3==1){data[data[i+3]+1] <- res}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- res}
      pas=4
    }
    
    if (action %in% c('8','08')){ #test egalité
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1==param2){ res=1}else{res=0}
      if (mode_param3==0){data[data[i+3]+1] <- res}
      if (mode_param3==1){data[data[i+3]+1] <- res}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- res}
      pas=4
    }
    
    if (action %in% c('9','09')){ # modif relative base
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      relative_base <-relative_base+param1
      pas=2
    }
    
    if (action==99){
      GO=FALSE
      STOP="FEU ROUGE" #met l'amplifier en arret
    }
    
    i=i+pas
    if (length(inputs)==0 & str_sub(data[i],nchar(data[i])-1,nchar(data[i]))%in% c('3','03')){GO=FALSE}
    if (length(inputs)>0){ if (all(is.na(inputs)) & str_sub(data[i],nchar(data[i])-1,nchar(data[i]))%in% c('3','03')){GO=FALSE}
      #met l'amplifier en pause si l'input est vide et qu'il va être requis
    }
  }
  
  #on sauvegarde l'output, le data, et le i pour savoir ou on est rendu; + inputs pour savoir ceux qui restent à utiliser
  #ajout d'un voyant lumineux STOP qui indique que l'amplifier cesse de fonctionner
  sortie = list(outputs,data,i,inputs, STOP,relative_base)
  return(sortie)
}


#### Part 1 ----
res=programme_intcode7(data,c(), 1,0)

instructions=res[[1]]
grille = matrix('_',nrow=65,ncol=40)
ins=1
i=1
j=1
for (ins in 1:length(instructions)){
  instruction=instructions[ins]
  if (instruction=='35'){grille[i,j] <- "#";j=j+1}
  if (instruction=='46'){grille[i,j] <- ".";j=j+1}
  if (instruction=='94'){grille[i,j] <- "^";j=j+1}
  if (instruction=='10'){i=i+1;j=1}
}


#recherche des croisements
sol=c()
for (i in 2:64){
  for (j in 2:39){
    if (grille[i,j]=='#' &  grille[i+1,j]=='#' &   grille[i-1,j]=='#' & grille[i,j+1]=='#' & grille[i,j-1]=='#'){
      grille[i,j] <- 'O'
      sol=c(sol,(i-1)*(j-1))
    }
  }
}

sum(sol)



#### Part 2 ----
# main=c("R6","R6","R8","L10","L4", A
#        "R6","L10","R8",C
#        "R6","L10","R8",C
#        "R6","R6","R8","L10","L4", A
#        "L4","L12","L6","L10", B
#        "R6","R6","R8","L10","L4", A
#        "L4","L12","R6","L10", B
#        "R6","R6","R8","L10","L4", A
#        "L4","L12","R6","L10", B
#        "R6","L10","R8")C

a=c("R","6","R","6","R","8","L","10","L","4")
b=c("L","4","L","12","L","6","L","10")
c=c("R","6","L","10","R","8")

A=c(82,44,54,44,82,44,54,44,82,44,56,44,76,44,49,48,44,76,44,52,10)
C=c(82,44,54,44,76,44,49,48,44,82,44,56,10)
B=c(76,44,52,44,76,44,49,50,44,82,44,54,44,76,44,49,48,10)
main=c(65,44,67,44,67,44,65,44,66,44,65,44,66,44,65,44,66,44,67,10)


data <- unlist(read_csv("input/input17.txt",  col_names = FALSE))  
data[1]=2

res2=programme_intcode7(data,c(),1,0)
table(res2[[1]]) #demande main
res3=programme_intcode7(res2[[2]],main,res2[[3]],res2[[6]])
table(res3[[1]]) #demande function A
res4=programme_intcode7(res3[[2]],A,res3[[3]],res3[[6]]) 
table(res4[[1]]) #demande function B
res5=programme_intcode7(res4[[2]],B,res4[[3]],res4[[6]])
table(res5[[1]]) #demande function C
res6=programme_intcode7(res5[[2]],C,res5[[3]],res5[[6]])
table(res6[[1]]) #Continuous video feed? y/n
res7=programme_intcode7(res6[[2]],c(121,10),res6[[3]],res6[[6]])
table(res7[[1]]) #output
tail(res7[[1]],100)
