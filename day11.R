################################
#-------- 11 Decembre ---------#
################################
source("script.R")
options(digits = 20)




#### programme Intcode V6 ----

# data: liste des entiers du jeu de données
# inputs: séquence des inputs
#i: indice de départ (en général i=1 quand on commence)
# rb: relative_base (en général rb=0 quand on commence)
# ce programme s'arrête s'il reçoit l'opcode 99 et store les outputs


programme_intcode6=function(data, inputs,indice_i,rb){
  #agrandir le data en mettant des zéros au bout
  data <- c(data,rep(0,10000))
  i=indice_i
  GO=TRUE
  STOP="FEU VERT"
  relative_base=rb
  outputs=c()

  while(GO){
    opcode=data[i]
    
    action=str_sub(opcode,nchar(opcode)-1,nchar(opcode))
    if (nchar(opcode)>2){mode_param1=str_sub(opcode,nchar(opcode)-2,nchar(opcode)-2) }else{mode_param1='0'}
    if (nchar(opcode)>3){mode_param2=str_sub(opcode,nchar(opcode)-3,nchar(opcode)-3) }else{mode_param2='0'}
    if (nchar(opcode)>4){mode_param3=str_sub(opcode,nchar(opcode)-4,nchar(opcode)-4) }else{mode_param3='0'}
    
    if (action %in% c('1','01')){ #addition
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (mode_param3==0){data[data[i+3]+1] <- param1+param2}
      if (mode_param3==1){data[data[i+3]+1] <- param1+param2}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- param1+param2 }
      pas=4
    }
    if (action %in% c('2','02')){ #multiplication
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (mode_param3==0){data[data[i+3]+1] <- param1*param2}
      if (mode_param3==1){data[data[i+3]+1] <- param1*param2}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- param1*param2 }
      pas=4
    }
    
    if (action %in% c('3','03')){ #input
      if (mode_param1==0){param1=data[i+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=relative_base+data[i+1]}
      if (length(inputs)==0){print("input manquant !!!")}
      data[param1+1] <- inputs[1]; #on utilise le premier input de la reserve
      pas=2
      # on supprime l'input utilisé,on conserve ceux qui restent en stock
      inputs= inputs[-1]
    }
    
    if (action %in% c('4','04')){ #output
      if (mode_param1==0){param1=data[data[i+1]+1] }
      if (mode_param1==1){param1=data[i+1] }
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      # print(param1)
      outputs=c(outputs,param1)
      pas=2
      #GO=FALSE    
    }
    
    if (action %in% c('5','05')){ #jump si != 0
      if (mode_param1==0){param1=data[data[i+1]+1] }
      if (mode_param1==1){param1=data[i+1] }
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1 != 0){ pas=-i+param2+1}else{pas=3}
    }
    
    if (action %in% c('6','06')){ #jump si == 0
      if (mode_param1==0){param1=data[data[i+1]+1] }
      if (mode_param1==1){param1=data[i+1] }
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1 == 0){ pas=-i+param2+1}else{pas=3}
    }
    
    if (action %in% c('7','07')){ #test inf
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1<param2){ res=1}else{res=0}
      if (mode_param3==0){data[data[i+3]+1] <- res}
      if (mode_param3==1){data[data[i+3]+1] <- res}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- res}
      pas=4
    }
    
    if (action %in% c('8','08')){ #test egalité
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      if (mode_param2==0){param2=data[data[i+2]+1]}
      if (mode_param2==1){param2=data[i+2]}
      if (mode_param2==2){param2=data[relative_base+data[i+2]+1] }
      if (param1==param2){ res=1}else{res=0}
      if (mode_param3==0){data[data[i+3]+1] <- res}
      if (mode_param3==1){data[data[i+3]+1] <- res}
      if (mode_param3==2){data[relative_base+data[i+3]+1] <- res}
      pas=4
    }
    
    if (action %in% c('9','09')){ # modif relative base
      if (mode_param1==0){param1=data[data[i+1]+1]}
      if (mode_param1==1){param1=data[i+1]}
      if (mode_param1==2){param1=data[relative_base+data[i+1]+1] }
      relative_base <-relative_base+param1
      pas=2
    }
    
    if (action==99){
      GO=FALSE
      STOP="FEU ROUGE" #met l'amplifier en arret
    }
    
    i=i+pas
    if (length(outputs)==2){GO=FALSE}   #met l'amplifier en pause si 2 outputs sont sortis
  }
  
  #on sauvegarde l'output, le data, et le i pour savoir ou on est rendu; + inputs pour savoir ceux qui restent à utiliser
  #ajout d'un voyant lumineux STOP qui indique que l'amplifier cesse de fonctionner
  sortie = list(outputs,data,i,inputs, STOP,relative_base)
  return(sortie)
}


#### Liste des instructions du Robot ----


#Initialisation
data=c(3,8,1005,8,314,1106,0,11,0,0,0,104,1,104,0,3,8,1002,8,-1,10,1001,10,1,10,4,10,108,1,8,10,4,10,1002,8,1,28,2,2,16,10,1,1108,7,10,1006,0,10,1,5,14,10,3,8,102,-1,8,10,101,1,10,10,4,10,108,1,8,10,4,10,102,1,8,65,1006,0,59,2,109,1,10,1006,0,51,2,1003,12,10,3,8,102,-1,8,10,1001,10,1,10,4,10,108,1,8,10,4,10,1001,8,0,101,1006,0,34,1,1106,0,10,1,1101,17,10,3,8,102,-1,8,10,101,1,10,10,4,10,1008,8,0,10,4,10,1001,8,0,135,3,8,1002,8,-1,10,101,1,10,10,4,10,108,0,8,10,4,10,1001,8,0,156,3,8,1002,8,-1,10,101,1,10,10,4,10,108,0,8,10,4,10,1001,8,0,178,1,108,19,10,3,8,102,-1,8,10,101,1,10,10,4,10,108,0,8,10,4,10,1002,8,1,204,1,1006,17,10,3,8,102,-1,8,10,101,1,10,10,4,10,108,1,8,10,4,10,102,1,8,230,1006,0,67,1,103,11,10,1,1009,19,10,1,109,10,10,3,8,102,-1,8,10,101,1,10,10,4,10,1008,8,0,10,4,10,101,0,8,268,3,8,102,-1,8,10,101,1,10,10,4,10,1008,8,1,10,4,10,1002,8,1,290,2,108,13,10,101,1,9,9,1007,9,989,10,1005,10,15,99,109,636,104,0,104,1,21101,48210224024,0,1,21101,0,331,0,1105,1,435,21101,0,937264165644,1,21101,0,342,0,1105,1,435,3,10,104,0,104,1,3,10,104,0,104,0,3,10,104,0,104,1,3,10,104,0,104,1,3,10,104,0,104,0,3,10,104,0,104,1,21101,235354025051,0,1,21101,389,0,0,1105,1,435,21102,29166169280,1,1,21102,400,1,0,1105,1,435,3,10,104,0,104,0,3,10,104,0,104,0,21102,709475849060,1,1,21102,1,423,0,1106,0,435,21102,868498428684,1,1,21101,434,0,0,1105,1,435,99,109,2,21201,-1,0,1,21101,0,40,2,21102,1,466,3,21101,456,0,0,1105,1,499,109,-2,2105,1,0,0,1,0,0,1,109,2,3,10,204,-1,1001,461,462,477,4,0,1001,461,1,461,108,4,461,10,1006,10,493,1101,0,0,461,109,-2,2106,0,0,0,109,4,2102,1,-1,498,1207,-3,0,10,1006,10,516,21102,1,0,-3,21201,-3,0,1,21201,-2,0,2,21102,1,1,3,21102,535,1,0,1106,0,540,109,-4,2106,0,0,109,5,1207,-3,1,10,1006,10,563,2207,-4,-2,10,1006,10,563,21202,-4,1,-4,1106,0,631,21201,-4,0,1,21201,-3,-1,2,21202,-2,2,3,21101,582,0,0,1105,1,540,22102,1,1,-4,21102,1,1,-1,2207,-4,-2,10,1006,10,601,21101,0,0,-1,22202,-2,-1,-2,2107,0,-3,10,1006,10,623,22102,1,-1,1,21101,623,0,0,105,1,498,21202,-2,-1,-2,22201,-4,-2,-4,109,-5,2105,1,0)
input_instruction <- 1;x[[2]] <-data;
x<-list()
x[[2]] <- data;
x[[3]] <- 1;
x[[6]] <- 0;
FEU <-"FEU VERT";



#### Gestion des déplacements du robot ----

df=data.frame(x=0,y=0,color=0,  actual=TRUE, orientation='N',nb_passages=1, stringsAsFactors = F)
cpt=0
while(FEU=="FEU VERT"){
x <-programme_intcode6(x[[2]],input_instruction,x[[3]],x[[6]]);
cpt=cpt+1
print(cpt)
  new_color<-x[[1]][1]
  new_direction<-x[[1]][2]
  FEU <-x[[5]];
  
df$color[df$actual==TRUE]<- new_color # on peint la case que le robot va quitter


#Gestion du déplacement du robot
new_orientation<-case_when(
  (new_direction==0 & df$orientation[df$actual==TRUE] == 'N') ~ 'W',
  (new_direction==0 & df$orientation[df$actual==TRUE] == 'S') ~ 'E',
  (new_direction==0 & df$orientation[df$actual==TRUE] == 'E') ~ 'N',
  (new_direction==0 & df$orientation[df$actual==TRUE] == 'W') ~ 'S',
  (new_direction==1 & df$orientation[df$actual==TRUE] == 'N') ~ 'E',
  (new_direction==1 & df$orientation[df$actual==TRUE] == 'S') ~ 'W',
  (new_direction==1 & df$orientation[df$actual==TRUE] == 'E') ~ 'S',
  (new_direction==1 & df$orientation[df$actual==TRUE] == 'W') ~ 'N'
)

new_x<-as.numeric(df$x[df$actual==TRUE])
new_y<-as.numeric(df$y[df$actual==TRUE])
new_x<-case_when(
  (new_orientation == 'E') ~ new_x+1,
  (new_orientation == 'W') ~ new_x-1,
  TRUE ~ new_x
)
new_y<-case_when(
  (new_orientation == 'N') ~ new_y+1,
  (new_orientation == 'S') ~ new_y-1,
  TRUE ~ new_y
)

# si la case a deja ete visitee
if (any(df$x==new_x & df$y==new_y )){
  df$actual <- FALSE # toutes les cases ne sont pas l'actuelle sauf...
  df$actual[df$x==new_x & df$y==new_y] <- TRUE
  df$orientation <- NA
  df$orientation[df$x==new_x & df$y==new_y] <- new_orientation
  df$nb_passages[df$x==new_x & df$y==new_y] <- as.numeric(df$nb_passages[df$x==new_x & df$y==new_y]) +1
 
}else{
# si la case n'a jamais ete visitee
  df$orientation[df$actual==TRUE] <- NA
  df$actual[df$actual==TRUE] <- FALSE
  df<-rbind(df,c(new_x,new_y,0,TRUE,new_orientation, 1))
}

input_instruction <- as.numeric(df$color[df$actual==TRUE])

}

## Dessin sur une grille
grille= matrix('.',nrow=200,ncol=200)
grille[100,100]<-'####'


for (iter in 1:length(df$x)){
 if (df[iter,]$color==1){
   i<-as.numeric(df[iter,]$x) +100
   j<-as.numeric(df[iter,]$y) +100
   grille[i,j] <- "####"
 } 
  
}
